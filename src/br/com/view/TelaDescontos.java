package br.com.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;

public class TelaDescontos extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void telaDescontos() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaDescontos frame = new TelaDescontos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaDescontos() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 453, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTaxasMinimasPor = new JLabel("TAXAS M\u00CDNIMAS POR RAMO DE ATIVIDADE");
		lblTaxasMinimasPor.setFont(new Font("Arial", Font.BOLD, 16));
		lblTaxasMinimasPor.setBounds(35, 31, 379, 31);
		contentPane.add(lblTaxasMinimasPor);
		
		JLabel lblSade = new JLabel("Sa\u00FAde");
		lblSade.setFont(new Font("Arial", Font.PLAIN, 20));
		lblSade.setBounds(44, 86, 160, 14);
		contentPane.add(lblSade);
		
		JLabel lblEducao = new JLabel("Educa\u00E7\u00E3o");
		lblEducao.setFont(new Font("Arial", Font.PLAIN, 20));
		lblEducao.setBounds(44, 122, 160, 35);
		contentPane.add(lblEducao);
		
		JLabel lblAlimentao = new JLabel("Alimenta\u00E7\u00E3o");
		lblAlimentao.setFont(new Font("Arial", Font.PLAIN, 20));
		lblAlimentao.setBounds(44, 157, 160, 46);
		contentPane.add(lblAlimentao);
		
		JLabel lblServios = new JLabel("Servi\u00E7os");
		lblServios.setFont(new Font("Arial", Font.PLAIN, 20));
		lblServios.setBounds(44, 214, 177, 31);
		contentPane.add(lblServios);
		
		JLabel lblEntretenimento = new JLabel("Entretenimento");
		lblEntretenimento.setFont(new Font("Arial", Font.PLAIN, 20));
		lblEntretenimento.setBounds(44, 264, 133, 14);
		contentPane.add(lblEntretenimento);
		
		JLabel label = new JLabel("3%");
		label.setFont(new Font("Arial", Font.PLAIN, 20));
		label.setBounds(295, 80, 46, 27);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("5%");
		label_1.setFont(new Font("Arial", Font.PLAIN, 20));
		label_1.setBounds(295, 122, 46, 24);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("7%");
		label_2.setFont(new Font("Arial", Font.PLAIN, 20));
		label_2.setBounds(295, 165, 46, 30);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("9%");
		label_3.setFont(new Font("Arial", Font.PLAIN, 20));
		label_3.setBounds(295, 218, 46, 22);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("10%");
		label_4.setFont(new Font("Arial", Font.PLAIN, 20));
		label_4.setBounds(295, 264, 46, 14);
		contentPane.add(label_4);
	}

}
