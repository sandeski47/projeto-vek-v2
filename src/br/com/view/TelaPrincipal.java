package br.com.view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import br.com.controller.vek.Principal;
import br.com.database.GenerateCSV;

public class TelaPrincipal extends JFrame {
	public static String txtClientesCadastrados = "";
	private JPanel contentPane;
	private JTextField pDescontoCredito;
	private JTextField pTaxaCreditoConcorrente;
	private JTextField pDescontoDebitoConcorrente;
	private JTextField pTaxaDebitoConcorrente;
	private JTextField pEmail;
	private JTextField pTelefone;
	
	

	/**
	 * Launch the application.
	 */
	public static void telaPrincipal(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 668, 764);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(574, 92, 1, 1);
		
		JDesktopPane desktopPane_1 = new JDesktopPane();
		desktopPane_1.setBounds(574, 148, 1, 1);
		
		JLabel label = new JLabel();
		label.setBounds(249, 43, 166, 43);
		label.setText("SIELO");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Arial", Font.BOLD, 36));
		
		JLabel lblEmpresaAtual = new JLabel();
		lblEmpresaAtual.setBounds(80, 162, 143, 15);
		lblEmpresaAtual.setText("* Empresa Atual: ");
		lblEmpresaAtual.setMaximumSize(new Dimension(0, 0));
		lblEmpresaAtual.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblTelefone = new JLabel();
		lblTelefone.setBounds(80, 259, 188, 15);
		lblTelefone.setText("* Telefone: ");
		lblTelefone.setMaximumSize(new Dimension(0, 0));
		lblTelefone.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblEmailopcional = new JLabel();
		lblEmailopcional.setBounds(80, 291, 188, 15);
		lblEmailopcional.setText("  E-MAIL ");
		lblEmailopcional.setMaximumSize(new Dimension(0, 0));
		lblEmailopcional.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblRamoDe = new JLabel();
		lblRamoDe.setBounds(80, 331, 188, 15);
		lblRamoDe.setText("* Ramo de Atividade: ");
		lblRamoDe.setMaximumSize(new Dimension(0, 0));
		lblRamoDe.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblTaxaDeDbito = new JLabel();
		lblTaxaDeDbito.setBounds(80, 360, 209, 15);
		lblTaxaDeDbito.setText("* Taxa de D\u00E9bito Concorrente:");
		lblTaxaDeDbito.setMaximumSize(new Dimension(0, 0));
		lblTaxaDeDbito.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblDescontoDbito = new JLabel();
		lblDescontoDbito.setBounds(80, 397, 188, 15);
		lblDescontoDbito.setText("* Desconto D\u00E9bito %: ");
		lblDescontoDbito.setMaximumSize(new Dimension(0, 0));
		lblDescontoDbito.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblTaxaDeCrdito = new JLabel();
		lblTaxaDeCrdito.setBounds(80, 433, 209, 15);
		lblTaxaDeCrdito.setText("* Taxa de Cr\u00E9dito Concorrente: ");
		lblTaxaDeCrdito.setMaximumSize(new Dimension(0, 0));
		lblTaxaDeCrdito.setFont(new Font("Arial", Font.PLAIN, 14));
		
		JLabel lblDescontoCrdito = new JLabel("* Desconto Cr\u00E9dito % :");
		lblDescontoCrdito.setBounds(80, 466, 188, 15);
		lblDescontoCrdito.setFont(new Font("Arial", Font.PLAIN, 14));
		
		pDescontoCredito = new JTextField();
		pDescontoCredito.setBounds(387, 464, 188, 29);
		pDescontoCredito.setColumns(10);
		
		pTaxaCreditoConcorrente = new JTextField();
		pTaxaCreditoConcorrente.setBounds(387, 427, 188, 28);
		
		pDescontoDebitoConcorrente = new JTextField();
		pDescontoDebitoConcorrente.setBounds(387, 390, 188, 31);
		
		pTaxaDebitoConcorrente = new JTextField();
		pTaxaDebitoConcorrente.setBounds(387, 353, 188, 31);
		
		pEmail = new JTextField();
		pEmail.setBounds(387, 285, 188, 28);
		
		pTelefone = new JTextField();
		pTelefone.setBounds(387, 248, 188, 30);
		pTelefone.setToolTipText("");
		
		JComboBox listaEmpresaAtual = new JComboBox();
		listaEmpresaAtual.setBounds(387, 160, 188, 21);
		listaEmpresaAtual.setFont(new Font("Arial", Font.BOLD, 12));
        listaEmpresaAtual.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
        		
       "SELECIONE UMA EMPRESA", "PAGSEGURO", "SUMUP", "STELO", "CIELO", "IZZETLE", "MERCADO PAGO" }));
		
		JFormattedTextField pCPFCNPJ = new JFormattedTextField();
		pCPFCNPJ.setBounds(387, 187, 188, 32);
		
		JComboBox listaRamoAtividade = new JComboBox();
		listaRamoAtividade.setBounds(387, 326, 188, 21);
		listaRamoAtividade.setFont(new Font("Arial", Font.BOLD, 12));
		listaRamoAtividade.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
        		
			       "SELECIONE UM RAMO", "SAUDE", "EDUCA��O", "ALIMENTA��O", "SERVI�OS", "ENTRETENIMENTO" }));
		
		
		
		
		JButton button_8 = new JButton();
		button_8.setBounds(223, 559, 209, 27);
		button_8.addActionListener(new ActionListener() {
			//CLICOU NO SUBMETER
			public void actionPerformed(ActionEvent arg0) {

				java.util.Date d = new Date();
				String data = java.text.DateFormat.getDateInstance(DateFormat.MEDIUM).format(d);
				System.out.println(data);
				double valorComDescontoDebito = 0;
				double valorComDescontoCredito = 0;
				System.out.println("Clicou em Submeter");
				if(listaEmpresaAtual.getSelectedIndex() != 0){
					if(pCPFCNPJ.getText().length() >= 10){
						if(pTelefone.getText().length() >= 11){
							if(listaRamoAtividade.getSelectedIndex() != 0){
								if(pTaxaDebitoConcorrente.getText().length() >0){
									if(pDescontoDebitoConcorrente.getText().length() > 0){
										if(pTaxaCreditoConcorrente.getText().length() >0){
											if(pDescontoCredito.getText().length() > 0){
												Principal pc = new Principal();
												
												switch (listaRamoAtividade.getSelectedIndex()) {
												case 1:
													valorComDescontoDebito = pc.calculaPorcentagem(Double.parseDouble(pTaxaDebitoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoDebitoConcorrente.getText()));
													valorComDescontoCredito = pc.calculaPorcentagem(Double.parseDouble(pTaxaCreditoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoCredito.getText()));
													Double.parseDouble(pTaxaCreditoConcorrente.getText());
													if(valorComDescontoDebito > pc.TAXA_SAUDE){
														JOptionPane.showMessageDialog(null, "PROPOSTA ACEITA");
														Principal.lista.add(listaEmpresaAtual.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pCPFCNPJ.getText());
														Principal.lista.add(";");
														Principal.lista.add(listaRamoAtividade.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pTelefone.getText());
														Principal.lista.add(";");
														
														if(pEmail.getText().length() < 1){
															Principal.lista.add("Sem Email");
															Principal.lista.add(";");
														}else{
															Principal.lista.add(pEmail.getText());
															Principal.lista.add(";");
														}
														Principal.lista.add(pTaxaDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoDebito));
														Principal.lista.add(";");
														Principal.lista.add(pTaxaCreditoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoCredito.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoCredito));
														Principal.lista.add(";");
														Principal.lista.add(data);
														Principal.lista.add("\n");
														
														txtClientesCadastrados += "EMPRESA Concorrente : " + listaEmpresaAtual.getSelectedItem().toString()+ "\n";
														txtClientesCadastrados += "CPF : " + pCPFCNPJ.getText() + "\n";
														txtClientesCadastrados += "Ramo da Empresa: " + listaEmpresaAtual.getSelectedItem().toString() + "\n";
														if(pEmail.getText().length() < 1){
															txtClientesCadastrados +="EMAIL:   Sem Email \n";
														}else{
															txtClientesCadastrados +="EMAIL:  " + pEmail.getText() + "\n";
														}
														txtClientesCadastrados += "Taxa de Debito Concorrente : "+pTaxaDebitoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de D�bito :  " + pDescontoDebitoConcorrente.getText() + "\n";
														txtClientesCadastrados += "Valor com o Desconto : " + String.valueOf(valorComDescontoDebito) + "\n";
														txtClientesCadastrados += "Taxa de Cr�dito Concorrente : "+pTaxaCreditoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de Cr�dito :  " + pDescontoCredito.getText() + "\n";
														txtClientesCadastrados += "Valor de Cr�dito com o Desconto : " + String.valueOf(valorComDescontoCredito) + "\n";
														txtClientesCadastrados += "Data do Contrato : " + data + "\n\n";
														
													}else{
														JOptionPane.showMessageDialog(null, "PROPOSTA REJEITADA \n A taxa m�nima de Sa�de � :" + pc.TAXA_SAUDE);
														
													}
													break;
												case 2:
													valorComDescontoDebito = pc.calculaPorcentagem(Double.parseDouble(pTaxaDebitoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoDebitoConcorrente.getText()));
													valorComDescontoCredito = pc.calculaPorcentagem(Double.parseDouble(pTaxaCreditoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoCredito.getText()));
													Double.parseDouble(pTaxaCreditoConcorrente.getText());
													if(valorComDescontoDebito > pc.TAXA_EDUCACAO){
														JOptionPane.showMessageDialog(null, "PROPOSTA ACEITA");
														Principal.lista.add(listaEmpresaAtual.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pCPFCNPJ.getText());
														Principal.lista.add(";");
														Principal.lista.add(listaRamoAtividade.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pTelefone.getText());
														Principal.lista.add(";");
														if(pEmail.getText().length() < 1){
															Principal.lista.add("Sem Email");
															Principal.lista.add(";");
														}else{
															Principal.lista.add(pEmail.getText());
															Principal.lista.add(";");
														}
														Principal.lista.add(pTaxaDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoDebito));
														Principal.lista.add(";");
														Principal.lista.add(pTaxaCreditoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoCredito.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoCredito));
														Principal.lista.add(";");
														Principal.lista.add(data );
														Principal.lista.add("\n");
														
														txtClientesCadastrados += "EMPRESA Concorrente : " + listaEmpresaAtual.getSelectedItem().toString()+ "\n";
														txtClientesCadastrados += "CPF : " + pCPFCNPJ.getText() + "\n";
														txtClientesCadastrados += "Ramo da Empresa: " + listaEmpresaAtual.getSelectedItem().toString() + "\n";
														if(pEmail.getText().length() < 1){
															txtClientesCadastrados +="EMAIL:   Sem Email \n";
														}else{
															txtClientesCadastrados +="EMAIL:  " + pEmail.getText() + "\n";
														}
														txtClientesCadastrados += "Taxa de Debito Concorrente : "+pTaxaDebitoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de D�bito :  " + pDescontoDebitoConcorrente.getText() + "\n";
														txtClientesCadastrados += "Valor com o Desconto : " + String.valueOf(valorComDescontoDebito) + "\n";
														txtClientesCadastrados += "Taxa de Cr�dito Concorrente : "+pTaxaCreditoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de Cr�dito :  " + pDescontoCredito.getText() + "\n";
														txtClientesCadastrados += "Valor de Cr�dito com o Desconto : " + String.valueOf(valorComDescontoCredito) + "\n";
														txtClientesCadastrados += "Data do Contrato : " + data + "\n\n";
													}else{
														JOptionPane.showMessageDialog(null, "PROPOSTA REJEITADA \n A taxa m�nima de Educa��o � :" + pc.TAXA_EDUCACAO);
													}
													break;
												case 3:
													valorComDescontoDebito = pc.calculaPorcentagem(Double.parseDouble(pTaxaDebitoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoDebitoConcorrente.getText()));
													valorComDescontoCredito = pc.calculaPorcentagem(Double.parseDouble(pTaxaCreditoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoCredito.getText()));
													Double.parseDouble(pTaxaCreditoConcorrente.getText());
													if(valorComDescontoDebito > pc.TAXA_ALIMENTACAO){
														JOptionPane.showMessageDialog(null, "PROPOSTA ACEITA");
														Principal.lista.add(listaEmpresaAtual.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pCPFCNPJ.getText());
														Principal.lista.add(";");
														Principal.lista.add(listaRamoAtividade.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pTelefone.getText());
														Principal.lista.add(";");
														
														if(pEmail.getText().length() < 1){
															Principal.lista.add("Sem Email");
															Principal.lista.add(";");
														}else{
															Principal.lista.add(pEmail.getText());
															Principal.lista.add(";");
														}
														Principal.lista.add(pTaxaDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoDebito));
														Principal.lista.add(";");
														Principal.lista.add(pTaxaCreditoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoCredito.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoCredito));
														Principal.lista.add(";");
														Principal.lista.add(data );
														Principal.lista.add("\n");
														
														txtClientesCadastrados += "EMPRESA Concorrente : " + listaEmpresaAtual.getSelectedItem().toString()+ "\n";
														txtClientesCadastrados += "CPF : " + pCPFCNPJ.getText() + "\n";
														txtClientesCadastrados += "Ramo da Empresa: " + listaEmpresaAtual.getSelectedItem().toString() + "\n";
														if(pEmail.getText().length() < 1){
															txtClientesCadastrados +="EMAIL:   Sem Email \n";
														}else{
															txtClientesCadastrados +="EMAIL:  " + pEmail.getText() + "\n";
														}
														txtClientesCadastrados += "Taxa de Debito Concorrente : "+pTaxaDebitoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de D�bito :  " + pDescontoDebitoConcorrente.getText() + "\n";
														txtClientesCadastrados += "Valor com o Desconto : " + String.valueOf(valorComDescontoDebito) + "\n";
														txtClientesCadastrados += "Taxa de Cr�dito Concorrente : "+pTaxaCreditoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de Cr�dito :  " + pDescontoCredito.getText() + "\n";
														txtClientesCadastrados += "Valor de Cr�dito com o Desconto : " + String.valueOf(valorComDescontoCredito) + "\n";
														txtClientesCadastrados += "Data do Contrato : " + data + "\n\n";
													}else{
														JOptionPane.showMessageDialog(null, "PROPOSTA REJEITADA \n A taxa m�nima de Alimenta��o � :" + pc.TAXA_ALIMENTACAO);
													}
													break;
												case 4:
													valorComDescontoDebito = pc.calculaPorcentagem(Double.parseDouble(pTaxaDebitoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoDebitoConcorrente.getText()));
													valorComDescontoCredito = pc.calculaPorcentagem(Double.parseDouble(pTaxaCreditoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoCredito.getText()));
													Double.parseDouble(pTaxaCreditoConcorrente.getText());
													if(valorComDescontoDebito > pc.TAXA_SERVICOS){
														JOptionPane.showMessageDialog(null, "PROPOSTA ACEITA");
														Principal.lista.add(listaEmpresaAtual.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pCPFCNPJ.getText());
														Principal.lista.add(";");
														Principal.lista.add((String) listaRamoAtividade.getSelectedItem());
														Principal.lista.add(";");
														Principal.lista.add(pTelefone.getText());
														Principal.lista.add(";");
														if(pEmail.getText().length() < 1){
															Principal.lista.add("Sem Email");
															Principal.lista.add(";");
														}else{
															Principal.lista.add(pEmail.getText());
															Principal.lista.add(";");
														}
														Principal.lista.add(pTaxaDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoDebito));
														Principal.lista.add(";");
														Principal.lista.add(pTaxaCreditoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoCredito.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoCredito));
														Principal.lista.add(";");
														Principal.lista.add(data );
														Principal.lista.add("\n");
														
														txtClientesCadastrados += "EMPRESA Concorrente : " + listaEmpresaAtual.getSelectedItem().toString()+ "\n";
														txtClientesCadastrados += "CPF : " + pCPFCNPJ.getText() + "\n";
														txtClientesCadastrados += "Ramo da Empresa: " + listaEmpresaAtual.getSelectedItem().toString() + "\n";
														if(pEmail.getText().length() < 1){
															txtClientesCadastrados +="EMAIL:   Sem Email \n";
														}else{
															txtClientesCadastrados +="EMAIL:  " + pEmail.getText() + "\n";
														}
														txtClientesCadastrados += "Taxa de Debito Concorrente : "+pTaxaDebitoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de D�bito :  " + pDescontoDebitoConcorrente.getText() + "\n";
														txtClientesCadastrados += "Valor com o Desconto : " + String.valueOf(valorComDescontoDebito) + "\n";
														txtClientesCadastrados += "Taxa de Cr�dito Concorrente : "+pTaxaCreditoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de Cr�dito :  " + pDescontoCredito.getText() + "\n";
														txtClientesCadastrados += "Valor de Cr�dito com o Desconto : " + String.valueOf(valorComDescontoCredito) + "\n";
														txtClientesCadastrados += "Data do Contrato : " + data + "\n\n";
													}else{
														JOptionPane.showMessageDialog(null, "PROPOSTA REJEITADA \n A taxa m�nima de Servi�os � :" + pc.TAXA_SERVICOS);
													}
													break;
												case 5:
													valorComDescontoDebito = pc.calculaPorcentagem(Double.parseDouble(pTaxaDebitoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoDebitoConcorrente.getText()));
													valorComDescontoCredito = pc.calculaPorcentagem(Double.parseDouble(pTaxaCreditoConcorrente.getText().toString()),
															Double.parseDouble(pDescontoCredito.getText()));
													Double.parseDouble(pTaxaCreditoConcorrente.getText());
													if(valorComDescontoDebito > pc.TAXA_ENTRETENIMENTO){
														JOptionPane.showMessageDialog(null, "PROPOSTA ACEITA");
														Principal.lista.add(listaEmpresaAtual.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pCPFCNPJ.getText());
														Principal.lista.add(";");
														Principal.lista.add( listaRamoAtividade.getSelectedItem().toString());
														Principal.lista.add(";");
														Principal.lista.add(pTelefone.getText());
														Principal.lista.add(";");
														
														if(pEmail.getText().length() < 1){
															Principal.lista.add("Sem Email;");
														}else{
															Principal.lista.add(pEmail.getText());
															Principal.lista.add(";");
														}
														Principal.lista.add(pTaxaDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoDebitoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoDebito));
														Principal.lista.add(";");
														Principal.lista.add(pTaxaCreditoConcorrente.getText());
														Principal.lista.add(";");
														Principal.lista.add(pDescontoCredito.getText());
														Principal.lista.add(";");
														Principal.lista.add(String.valueOf(valorComDescontoCredito));
														Principal.lista.add(";");
														Principal.lista.add(data );
														Principal.lista.add("\n");
														
														txtClientesCadastrados += "EMPRESA Concorrente : " + listaEmpresaAtual.getSelectedItem().toString()+ "\n";
														txtClientesCadastrados += "CPF : " + pCPFCNPJ.getText() + "\n";
														txtClientesCadastrados += "Ramo da Empresa: " + listaEmpresaAtual.getSelectedItem().toString() + "\n";
														if(pEmail.getText().length() < 1){
															txtClientesCadastrados +="EMAIL:   Sem Email \n";
														}else{
															txtClientesCadastrados +="EMAIL:  " + pEmail.getText() + "\n";
														}
														txtClientesCadastrados += "Taxa de Debito Concorrente : "+pTaxaDebitoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de D�bito :  " + pDescontoDebitoConcorrente.getText() + "\n";
														txtClientesCadastrados += "Valor com o Desconto : " + String.valueOf(valorComDescontoDebito) + "\n";
														txtClientesCadastrados += "Taxa de Cr�dito Concorrente : "+pTaxaCreditoConcorrente.getText()+ "\n";
														txtClientesCadastrados += "Desconto de Cr�dito :  " + pDescontoCredito.getText() + "\n";
														txtClientesCadastrados += "Valor de Cr�dito com o Desconto : " + String.valueOf(valorComDescontoCredito) + "\n";
														txtClientesCadastrados += "Data do Contrato : " + data + "\n\n";
													}else{
														JOptionPane.showMessageDialog(null, "PROPOSTA REJEITADA \n A taxa m�nima de Entretenimento � :" + pc.TAXA_ENTRETENIMENTO);
													}
													break;

												}
												
												
												
												
												
												
												
											}else{
												JOptionPane.showMessageDialog(null, "Corrija o Desconto de Cr�dito");
											}
										}else{
											JOptionPane.showMessageDialog(null, "Corrija a Taxa de Cr�dito Concorrente");
										}
									}else{
										JOptionPane.showMessageDialog(null, "Corrija o Desconto de D�bito");
									}
								}else{
									JOptionPane.showMessageDialog(null, "Corrija a Taxa de D�bito Concorrente");
								}
							}else{
								JOptionPane.showMessageDialog(null, "Selecione um Ramo de Atividade");
							}
						}else{
							JOptionPane.showMessageDialog(null, "Corrija o Telefone");
						}
					}else{
						JOptionPane.showMessageDialog(null,"Corrija o CPF ou CNPJ");
					}
				}else{
					JOptionPane.showMessageDialog(null, "Selecione uma empresa");
				}
				System.out.println(listaEmpresaAtual.getSelectedItem());
			}
		});
		button_8.setFont(new Font("Arial", Font.PLAIN, 15));
		button_8.setText("Submeter");
		
		JButton btnConsultarDescontos = new JButton("Consultar Descontos");
		btnConsultarDescontos.setBounds(223, 592, 209, 27);
		btnConsultarDescontos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaDescontos.telaDescontos();
				System.out.println("bot�o consultar clicado");
			}
		});
		btnConsultarDescontos.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JButton btnConsultaCadastros = new JButton("Consultar Cadastros");
		btnConsultaCadastros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Bot�o clientes Cadastrados Clicado");
				if(txtClientesCadastrados.length() > 0){
					JOptionPane.showMessageDialog(null, txtClientesCadastrados);
				}else{
					JOptionPane.showMessageDialog(null, "Nenhum cliente foi cadastrado ainda.");
				}
			}
		});
		btnConsultaCadastros.setBounds(223, 625, 209, 27);
		btnConsultaCadastros.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JButton btnGerarCSV = new JButton("GERAR CSV");
		btnGerarCSV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GenerateCSV gc = new GenerateCSV();
				try {
					gc.generateCsvFile();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("csv gerado, lista com "+ Principal.lista.size()+ " itens" );
			}
		});
		btnGerarCSV.setBounds(223, 658, 209, 27);
		btnGerarCSV.setFont(new Font("Arial", Font.PLAIN, 15));
		contentPane.setLayout(null);
		contentPane.add(desktopPane);
		contentPane.add(desktopPane_1);
		contentPane.add(lblEmpresaAtual);
		contentPane.add(listaEmpresaAtual);
		contentPane.add(lblTelefone);
		contentPane.add(pTelefone);
		contentPane.add(lblEmailopcional);
		contentPane.add(pEmail);
		contentPane.add(lblDescontoDbito);
		contentPane.add(pDescontoDebitoConcorrente);
		contentPane.add(lblDescontoCrdito);
		contentPane.add(pDescontoCredito);
		contentPane.add(pCPFCNPJ);
		contentPane.add(lblTaxaDeCrdito);
		contentPane.add(lblRamoDe);
		contentPane.add(lblTaxaDeDbito);
		contentPane.add(listaRamoAtividade);
		contentPane.add(pTaxaDebitoConcorrente);
		contentPane.add(pTaxaCreditoConcorrente);
		contentPane.add(label);
		contentPane.add(btnGerarCSV);
		contentPane.add(btnConsultaCadastros);
		contentPane.add(btnConsultarDescontos);
		contentPane.add(button_8);
		
		JLabel lblCpfcnpj = new JLabel("* CPF/CNPJ : ");
		lblCpfcnpj.setFont(new Font("Arial", Font.PLAIN, 14));
		lblCpfcnpj.setBounds(80, 188, 110, 29);
		contentPane.add(lblCpfcnpj);
	}
}
